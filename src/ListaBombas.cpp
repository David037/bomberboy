#include "ListaBombas.h"


ListaBombas::ListaBombas(void)
{
	cont = 0;
}


ListaBombas::~ListaBombas(void)
{
}

void ListaBombas::AgregarBomba(Vector_2D pos,double t)
{
	if (cont == MAX_BOMBAS)
		return;
	bombas [cont].SetPos(pos);
	bombas [cont].SetTiempo(t);
	cont++;


	
}

void ListaBombas::EliminarBomba()
{
	for (int i=0; i<cont-1; i++)
		bombas [i]  = bombas [i+1];
	cont--;
}

void ListaBombas::Coordenadas(Vector_2D *vector)
{
	
	for (int i = 0; i<cont; i++)
		vector [i] = bombas [i].Coordenada();

}