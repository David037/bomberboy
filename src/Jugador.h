#pragma once

#include <stdio.h>
#include "Personaje.h"



#define Max_Column	15
#define Max_Fil 16

class Jugador : public Personaje
{

public:
	Jugador(void);
	~Jugador(void);

	void Mueve(bool, int, char map [Max_Column] [Max_Fil]);


};
