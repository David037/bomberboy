#include "ListaEnemigos.h"


ListaEnemigos::ListaEnemigos(void)
{
}


ListaEnemigos::~ListaEnemigos(void)
{
}

void ListaEnemigos::AgregarEnemigo(int y, int x, int velocidad,int vidas)
{
	enemigo [cont].SetPos(y,x);
	enemigo [cont].SetVel(velocidad);
	enemigo [cont].SetVida(vidas);
	cont++;
}

void ListaEnemigos::EliminarEnemigo()
{
	for (int i = 0; i<cont; i++)
		if (enemigo [i].Muerto())
		{
			enemigo [i] = enemigo [cont];
			cont--;
			//i--;
		}
}

void ListaEnemigos::QuitaVida( Matriz mapa, double t)
{
	char map [Max_Column] [Max_Fil];

	for(int y=0;y<Max_Fil-1;y++)				
			for(int x=0;x<=Max_Column;x++)
				map[y][x]=mapa.GetValor(x,y);


	for (int i =0;i<cont;i++)
	{
		Vector_2D pos = enemigo [i].Coordenada();
		if (t > enemigo [i].getT0())
			if (map [pos.y] [pos.x] == '*')
			enemigo [i].QuitaVida(t);
		if(enemigo[i].Muerto())
		{
		enemigo [i] = enemigo [cont - 1];
			cont--;
		}
	}
}