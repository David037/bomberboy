#pragma once

#include <stdio.h>
#include <math.h>



#include "Graficos.h"
#include "Jugador.h"

#include "Vector_2D.h"
#include "ListaBombas.h"
#include "ListaFuegos.h"
#include "Matriz.h"
#include "ListaEnemigos.h"



class Mapa
{
private:
	char mapa [Max_Column] [Max_Fil];
	int estado;
		
	Graficos graficos;
	Jugador jugador;
	ListaEnemigos enemigos;
	ListaBombas bombas;
	ListaFuegos fuegos;
	

public:
	Mapa(void);
	~Mapa(void);

	void Inicializa();
	void Actualiza();	
	void Tecla(unsigned char key);
	Matriz GetMapa ();
	int getEstado(){return estado;}

	
};
