#pragma once
#include "Fuego.h"
#include "Matriz.h"

#define MAX_EXPLOSION 3

class ListaFuegos
{
private:
	Fuego fuego [5];
	int cont;
public:
	ListaFuegos(void);
	~ListaFuegos(void);

	Matriz EscribirMapa(Matriz);
	void AgregarFuego(Vector_2D );
	void EliminarFuego();
	void SetTiempo (double t){fuego [cont-1].SetTiempo(t);};
	int DevuelveCont (){return cont;};
	double Devuelvet0 (){return fuego [0].GetT0();};
};

