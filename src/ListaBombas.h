#pragma once

#include "Bomba.h"
#include "Vector_2D.h"

#define MAX_BOMBAS 5

class ListaBombas
{
private:

	Bomba bombas [MAX_BOMBAS];
	int cont;

public:
	ListaBombas(void);
	~ListaBombas(void);

	void AgregarBomba(Vector_2D pos, double);
	void EliminarBomba();
	void ContBombas ();
	void Coordenadas (Vector_2D *);
	int DevuelveCont () {return cont;}
	void DevuelveBomba (Bomba &b) {b = bombas [0];} 

	double Devuelvet0(){return bombas [0].Devuelvet0();}

};

