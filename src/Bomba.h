#pragma once
#include <glut.h>
#include <time.h>
#include "Vector_2D.h"

class Bomba
{
private:
	Vector_2D posicion;
	double t0;

public:
	Bomba(void);
	~Bomba(void);

	
	Vector_2D Coordenada();
	void SetPos (Vector_2D pos);
	double Devuelvet0 () {return t0;}
	void SetTiempo(double t){t0 = t;}


	friend class Jugador;

};

