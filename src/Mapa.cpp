#include "Mapa.h"


Mapa::Mapa(void)
{
	estado = 0;
	char map[Max_Column][Max_Fil]={
	"XXXXXXXXXXXXXXX",
	"X  cccccccc   X",
	"X X X X X X X X",
	"Xc   ccc  ccccX",
	"X X X X X XcXcX",
	"XcccccccccccccX",
	"XcX XcXcXcX X X",
	"X    ccccc c cX",
	"XcX XcX XcXcXcX",
	"Xc cccc ccccccX",
	"XcXcX X X X XcX",
	"X  cccccccccc X",
	"X XcX X X X X X",
	"Xcccccc   cc  X",
	"XXXXXXXXXXXXXXX",
	};

	

	for(int y=0;y<Max_Fil-1;y++)
		for(int x=0;x<Max_Column;x++)	
		{
			mapa [y][x] = map [y][x];
		}
}


Mapa::~Mapa(void)
{
}

void Mapa::Inicializa() 
{
	jugador.SetPos(1,1);

	enemigos.AgregarEnemigo(13,13,500,1);
	enemigos.AgregarEnemigo(13,7,300,1);
	enemigos.AgregarEnemigo(3,3,300,2);
	enemigos.AgregarEnemigo(9,3,500,1);
	enemigos.AgregarEnemigo(1,13,800,1);
	
}



void Mapa::Actualiza()
{
	Vector_2D posJug,posBombas [5];
	
																	
	posJug = jugador.Coordenada();

	bombas.Coordenadas(posBombas);

	for (int i = 0; i<enemigos.GetCont();i++)			
		if (clock() >  enemigos.GetT0(i)+enemigos.GetVel(i) )
			enemigos.Mueve(i,mapa);

	if (bombas.DevuelveCont()>0)						
		if (clock() >  bombas.Devuelvet0()+3000 )
		{	
			
			fuegos.AgregarFuego(posBombas [0]);
			fuegos.SetTiempo(clock());
			bombas.EliminarBomba();
		}

	
	for(int y=0;y<Max_Fil-1;y++)						//Limpiar posiciones anteriores
		for(int x=0;x<=Max_Column;x++)
			if(mapa[y][x]!='X' && mapa[y][x]!='c' )
				mapa[y][x]=' ';

	//Posicionamiento de los objetos en el mapa.

				
	for (int i =0 ; i<enemigos.GetCont() ;i++)			//Enemigos
		{
			Vector_2D pos=enemigos.GetPos(i);
			mapa [pos.y] [pos.x] = 'e';
		}
	if (!jugador.Muerto())								//Jugador
		mapa [posJug.y] [posJug.x] = 'J';

	if (!jugador.Muerto())								//Bombas
	for (int i=0; i<bombas.DevuelveCont(); i++)
			mapa [posBombas [i].y] [posBombas [i].x] = 'o';


	if (fuegos.DevuelveCont()>0)						//Fuego
		if (clock() >  fuegos.Devuelvet0()+1000 )
		{
			fuegos.EliminarFuego();
		}

		if (fuegos.DevuelveCont()>0)	
		{
			Matriz matriz;
			for(int x=0; x<15;x++)
				for(int y=0; y<15;y++)
					matriz.SetValor(x,y,mapa[y][x]);
			matriz = fuegos.EscribirMapa(matriz);
			for(int x=0; x<15;x++)
				for(int y=0; y<15;y++)
					mapa[y][x]=matriz.GetValor(x,y);
		}

		//Actualizacion estado de los personajes

		for(int y=0;y<Max_Fil-1;y++)				
			for(int x=0;x<=Max_Column;x++)
			{
				if (mapa [y] [x] == '*')
				{
					if (clock() > jugador.GetT0()+4000)
					{
						Vector_2D pos;
						pos.y = y;
						pos.x = x;

						if (pos == jugador.Coordenada())
							jugador.QuitaVida(clock());
					}
				}
			}
			


			for (int i =0; i<enemigos.GetCont();i++)
			{
				if (jugador.Coordenada() == enemigos.GetPos(i))
					if (clock() > jugador.GetT0()+4000)
						jugador.QuitaVida(clock());
			}

			Matriz m;
			for(int y=0;y<Max_Fil-1;y++)				
				for(int x=0;x<=Max_Column;x++)
				{
			
			for(int x=0; x<15;x++)
				for(int y=0; y<15;y++)
					m.SetValor(x,y,mapa[y][x]);

				}


			enemigos.QuitaVida(m, clock());

			Graficos::Dibuja(m,estado);

			if (jugador.Muerto())
				estado = 2;
			if (enemigos.GetCont() == 0)
				estado = 3;


}

Matriz Mapa::GetMapa()
{
	Matriz m;
			for(int y=0;y<Max_Fil-1;y++)				
				for(int x=0;x<=Max_Column;x++)
				{
			
			for(int x=0; x<15;x++)
				for(int y=0; y<15;y++)
					m.SetValor(x,y,mapa[y][x]);

				}

				return m;
}




void Mapa::Tecla(unsigned char key)
{
	/*Vector_2D posJug;

	posJug = jugador.Coordenada();*/
	if (estado == 1)
	{
	if (key == 'a' ||key == 'A' ){
		jugador.Mueve(0,-1,mapa);
	}
	if (key == 'd' ||key == 'D' ) {
		jugador.Mueve(0,1,mapa);
	}
	if (key == 'w' ||key == 'W' ){
		jugador.Mueve(1,-1,mapa);
	}
	if (key == 's'||key == 'S' ) {
		jugador.Mueve(1,1,mapa);
	}
	if (key == ' ') {
		if (!jugador.Muerto())
		bombas.AgregarBomba(jugador.Coordenada(),clock());
	}	
	}
	else if (estado==0)
		if (key == 'j')
			estado=1;

}
