#pragma once
#include "glut.h"
#include "Vector_2D.h"

#define Max_Column	15
#define Max_Fil 16

class Personaje
{
protected:
	Vector_2D posicion;
	Vector_2D posicionAnt;
	int velocidad;
	int vidas;
	double t0;

public:
	Personaje(void);
	~Personaje(void);

	
	Vector_2D Coordenada();
	void SetPos(int , int);
	void SetVel(int);
	int GetVel(){return velocidad;}
	void QuitaVida(double t){vidas--;	t0=t;}
	virtual double GetT0(){return t0;}
	bool Muerto();

};
