#include "Graficos.h"


Graficos::Graficos(void)
{
}


Graficos::~Graficos(void)
{
}


void Graficos::Dibuja(Matriz& map, int est)
{
	if (est == 1)
	{
		gluLookAt((Max_Column/2), -(Max_Fil/2)-0.5, 25,  // posicion del ojo
		(Max_Column/2), -(Max_Fil/2)-0.5, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);  
	char mapa [Max_Column][Max_Fil];

	for(int y=0;y<Max_Fil-1;y++)
		for(int x=0;x<Max_Column;x++)
			mapa [y][x] = map.GetValor(x,y);

	for(int y=0;y<Max_Fil-1;y++)				//Recorrido de la matriz
		for(int x=0;x<=Max_Column;x++)			
		{
			switch(mapa[y][x])
			{
			case 'X':							//Pared
				{
					glPushMatrix();
					glTranslatef(0,0,0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D,ETSIDI::getTexture("Imagenes/Muro.png").id);
					glDisable(GL_LIGHTING);
					glBegin(GL_POLYGON);
					glColor3f(1,1,1);

					glTexCoord2d(0,1);		glVertex2f(x,-y);
					glTexCoord2d(1,1);		glVertex2f(x+1,-y);
					glTexCoord2d(1,0);		glVertex2f(x+1,-y-1);
					glTexCoord2d(0,0);		glVertex2f(x,-y-1);

					glEnd();
					glEnable(GL_LIGHTING);
					glDisable(GL_TEXTURE_2D);
					break;
				}
			case 'J':					//Jugador
				{
					glPushMatrix();
					glTranslatef(0,0,0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D,ETSIDI::getTexture("Imagenes/Jugador.png").id);
					glDisable(GL_LIGHTING);
					glBegin(GL_POLYGON);
					glColor3f(1,1,1);

					glTexCoord2d(0,1);		glVertex2f(x,-y);
					glTexCoord2d(1,1);		glVertex2f(x+1,-y);
					glTexCoord2d(1,0);		glVertex2f(x+1,-y-1);
					glTexCoord2d(0,0);		glVertex2f(x,-y-1);

					glEnd();
					glEnable(GL_LIGHTING);
					glDisable(GL_TEXTURE_2D);


					break;
				}
			case 'c':					//Caja
				{
					glPushMatrix();
					glTranslatef(0,0,0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D,ETSIDI::getTexture("Imagenes/Caja2.png").id);
					glDisable(GL_LIGHTING);
					glBegin(GL_POLYGON);
					glColor3f(1,1,1);

					glTexCoord2d(0,1);		glVertex2f(x,-y);
					glTexCoord2d(1,1);		glVertex2f(x+1,-y);
					glTexCoord2d(1,0);		glVertex2f(x+1,-y-1);
					glTexCoord2d(0,0);		glVertex2f(x,-y-1);

					glEnd();
					glEnable(GL_LIGHTING);
					glDisable(GL_TEXTURE_2D);

					break;
				}
			case 'o':					//Bomba
				{
					glPushMatrix();
					glTranslatef(0,0,0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D,ETSIDI::getTexture("Imagenes/Bomba.png").id);
					glDisable(GL_LIGHTING);
					glBegin(GL_POLYGON);
					glColor3f(1,1,1);

					glTexCoord2d(0,1);		glVertex2f(x,-y);
					glTexCoord2d(1,1);		glVertex2f(x+1,-y);
					glTexCoord2d(1,0);		glVertex2f(x+1,-y-1);
					glTexCoord2d(0,0);		glVertex2f(x,-y-1);

					glEnd();
					glEnable(GL_LIGHTING);
					glDisable(GL_TEXTURE_2D);
					break;
				}						
			case 'e':					//Enemigo
				{
					glPushMatrix();
					glTranslatef(0,0,0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D,ETSIDI::getTexture("Imagenes/Enemigo.png").id);
					glDisable(GL_LIGHTING);
					glBegin(GL_POLYGON);
					glColor3f(1,1,1);

					glTexCoord2d(0,1);		glVertex2f(x,-y);
					glTexCoord2d(1,1);		glVertex2f(x+1,-y);
					glTexCoord2d(1,0);		glVertex2f(x+1,-y-1);
					glTexCoord2d(0,0);		glVertex2f(x,-y-1);

					glEnd();
					glEnable(GL_LIGHTING);
					glDisable(GL_TEXTURE_2D);
					break;
				}
			case '*':					//Fuego
				{
					glPushMatrix();
					glTranslatef(0,0,0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D,ETSIDI::getTexture("Imagenes/Fuego.png").id);
					glDisable(GL_LIGHTING);
					glBegin(GL_POLYGON);
					glColor3f(1,1,1);

					glTexCoord2d(0,1);		glVertex2f(x,-y);
					glTexCoord2d(1,1);		glVertex2f(x+1,-y);
					glTexCoord2d(1,0);		glVertex2f(x+1,-y-1);
					glTexCoord2d(0,0);		glVertex2f(x,-y-1);

					glEnd();
					glEnable(GL_LIGHTING);
					glDisable(GL_TEXTURE_2D);
					break;
				}
			};
		};
}

	else if (est == 0)
	{
		gluLookAt(0, 7.5, 30,     
			 0.0, 7.5, 0.0,      
			 0.0, 1.0, 0.0);     
		 ETSIDI::setTextColor(1,1,0);  
		 ETSIDI::setFont("fuentes/Bitwise.ttf",40); 
		 ETSIDI::printxy("Bomberman", -3,12);  
		 ETSIDI::setTextColor(1,1,1);  
		 ETSIDI::setFont("fuentes/Bitwise.ttf",12); 
		 ETSIDI::printxy("PULSE DE LA TECLA -J- PARA JUGAR", -10,7); 
		 ETSIDI::printxy("Sergio Sedano & David Roncero",2,1);
	}

	else if (est == 2)
	{
		gluLookAt(0, 27.5, 45,   
			 0.0, 27.5, 0.0,      
			 0.0, 1.0, 0.0);    
		 ETSIDI::setTextColor(1,1,0);  
		 ETSIDI::setFont("fuentes/Bitwise.ttf",40); 
		 ETSIDI::printxy("      Game Over", -13,32);  		 
	}

	else if (est == 3)
	{
		gluLookAt(0, 27.5, 45,   
			 0.0, 27.5, 0.0,      
			 0.0, 1.0, 0.0);     
		 ETSIDI::setTextColor(1,1,0);  
		 ETSIDI::setFont("fuentes/Bitwise.ttf",40); 
		 ETSIDI::printxy("      WINNER", -13,32);  		 
	}

}
