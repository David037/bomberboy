#pragma once

#include "Enemigo.h"
#include "Vector_2D.h"
#include "Matriz.h"

#define MAX_ENEMIGOS 5

class ListaEnemigos
{

	Enemigo enemigo [MAX_ENEMIGOS];
	int cont;
public:
	ListaEnemigos(void);
	~ListaEnemigos(void);

	void AgregarEnemigo(int y, int x, int velocidad, int vidas);
	void EliminarEnemigo();
	int GetCont(){return cont;}
	Vector_2D GetPos(int i){return enemigo [i].Coordenada();}
	double GetT0(int i){return enemigo [i].getT0();}
	int GetVel(int i){return enemigo [i].GetVel();}
	void Mueve(int i,char map [Max_Column] [Max_Fil]){enemigo [i].Mueve(map);}
	void QuitaVida (Matriz map, double t);



};

