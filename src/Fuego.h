#pragma once
#include "Vector_2D.h"


class Fuego
{
private:
	Vector_2D posicion;
	double t0;
public:
	Fuego(void);
	~Fuego(void);

	void SetPos (Vector_2D pos){posicion = pos;}
	Vector_2D GetPos (){return posicion;}
	void SetTiempo (double t) {t0 = t;}
	double GetT0(){return t0;}
};

