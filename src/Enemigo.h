#pragma once

#include <cstdlib>
#include <time.h>
#include "Personaje.h"


class Enemigo:public Personaje
{
public:
	Enemigo(void);
	~Enemigo(void);

	void Mueve(char map [Max_Column] [Max_Fil]);
	double getT0 (){return t0;}
	void SetVida(int v){vidas = v;}
	
};

